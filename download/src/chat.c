#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/epoll.h>

#include "download.h"	

struct linknode{
	char name[10];
	int csock;
	int port;
	char ip[30];
	struct linknode *next;
};
typedef struct linknode Link;
int num = 0;
Link *head = NULL;

int add_link(Link *pr)
{
	Link *pold,*p = head;
	if(head == NULL)
	{
		head = pr;
		return 0;
	}
	else
	{
		while(p!=NULL)
		{
			if(p->csock == pr->csock)
			{
				free(pr);
				num--;
				return 0;
			}
			pold = p;
			p = p->next;
		}
		pold ->next = pr;
	}
	return 0;
}

int main()
{
	struct epoll_event tep;
	struct epoll_event ep[1024];
	int sock = CreateTcpServe(8888);
	int efd = epoll_create(1024);
	if(efd ==-1)
	{
		return 0;
	}
	int nready;
	int connfd,ret;
	char str[20]={0};
	char buf[100]={0};
	char buff[1024]={0};
	unsigned int clilen;
	struct sockaddr_in cliaddr;
	tep.events = EPOLLIN;
	tep.data.fd = sock;
	epoll_ctl(efd,EPOLL_CTL_ADD,sock,&tep);

	while((nready = epoll_wait(efd,ep,1024,-1))>=0)
	{
		for(int i = 0;i < nready;i++)
		{
			if(!(ep[i].events & EPOLLIN))
				continue;
			if(ep[i].data.fd == sock)
			{
				clilen = sizeof(cliaddr);
				connfd = accept(sock,(struct sockaddr *)&cliaddr,&clilen);
/*				printf("receive from %s at PORT %d\n",
					inet_ntop(AF_INET,&cliaddr.sin_addr,str,sizeof(str)),
					ntohs(cliaddr.sin_port));*/
				Link *pr = (Link *)malloc(sizeof(Link));
				strcpy(pr->name,"client");
				//strcat(pr->name,num);
				//num++;
				strcpy(pr->ip,inet_ntop(AF_INET,&cliaddr.sin_addr,str,sizeof(str)));
				pr->port = ntohs(cliaddr.sin_port);
				pr->csock = connfd;
				pr->next = NULL;
				printf("csock:%d\n",pr->csock);
				add_link(pr);
				tep.events = EPOLLIN;
				tep.data.fd = connfd;
				epoll_ctl(efd,EPOLL_CTL_ADD,connfd,&tep);
			}
			else
			{
				int csock = ep[i].data.fd;
				memset(buf,0,sizeof(buf));
				ret = recv(csock,buf,sizeof(buf),0);
				if(ret <= 0)
				{
					epoll_ctl(efd,EPOLL_CTL_DEL,csock,NULL);
				}
				else
				{ 
					Link *p = head;
					printf("recv[%d]:%s\n",ret,buf);
					if(strcmp(buf,"quit")==0)
					{
						close(csock);
						epoll_ctl(efd,EPOLL_CTL_DEL,csock,NULL);
						continue;
					}
					else if(strncmp(buf,"rename",6)==0)
					{
						while(p!=NULL)
						{
							if(p->csock == csock)
							{
								strcpy(p->name,buf+7);
								break;
							}
							p = p->next;
						}
						//continue;
					}
					else if(strncmp(buf,"down",4)==0)
					{
						p = head;
						while(p!=NULL)
						{
							if(p->csock == csock)
							{
								sprintf(buff,"[%s][%s]",p->ip,buf + 5);
								break;
							}
							p = p->next;
						}
						pthread_t a_tid;
						pthread_create(&a_tid,NULL,download,buff);
					}
					memset(buff,0,sizeof(buff));
					sprintf(buff,"[%s][%s]:%s",p->name,p->ip,buf);
					p = head;
					while(p!=NULL)
					{
						if(p->csock == csock)
						{
							p = p->next;
							continue;
						}
						send(p->csock,buff,strlen(buff),0);
						p = p->next;
					}
				}
			}
		}
	}
}