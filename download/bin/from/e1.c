#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include <fcntl.h>
#include <dirent.h>

int CreateTcpServe(int port)
{
	int sock = socket(AF_INET,SOCK_STREAM,0);
	struct sockaddr_in ad;
	ad.sin_family = AF_INET;
	ad.sin_port = htons(port);
//	inet_pton(AF_INET,"192.168.0.106",&ad.sin_addr.s_addr);
	ad.sin_addr.s_addr = INADDR_ANY;

	int opt = 1;
	setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof(opt));

	if(bind(sock,(struct sockaddr *)&ad,sizeof(ad)))
	{
		perror("bind error\n");
		close(sock);
		return -1;
	}
	listen(sock,5);
	return sock;
}

void *fun(void *arg)
{
	pthread_detach(pthread_self());
	int sock = *(int *)arg;
	char buf[1024]={0};
	char filename[50]={0};
	int ret;
	DIR *dir;
	struct dirent *ptr;
//	struct stat buff;
	dir = opendir(".");
	while(ret = read(sock,buf,sizeof(buf))>0)
	{
		if(strcmp(buf,"list")==0)
		{
			printf("start\n");
			while(ptr = readdir(dir))
			{
				memset(buf,0,sizeof(buf));
				strcpy(buf,ptr->d_name);
//				printf("%s ",ptr->d_name);
				strcat(buf,"\n");
				send(sock,buf,1024,0);
			}
		}
		else if(strcmp(buf,"get")==0)
		{
			send(sock,"please input the name of file:\n",1024,0);
			read(sock,buf,sizeof(buf));
//			stat(buf,&buff);
//			printf("%10lu\n",buff);
//			send(sock,&buff,4,0);
			printf("%s\n",buf);
			int fd = open(buf,O_RDONLY);
			if(fd == 0)
			{
				send(sock,"you input wrong\n",1024,0);
				continue;
			}
			while((ret = read(fd,buf,1024))>0)
			{
				send(sock,buf,ret,0);
				memset(buf,0,sizeof(buf));
			}
			close(fd);
			printf("read over\n");
		}
		else if(strcmp(buf,"put")==0)
		{
			send(sock,"please input the name of file:\n",1024,0);
			read(sock,buf,sizeof(buf));
			memset(filename,0,sizeof(filename));
			strcpy(filename,"./from/");
			strcat(filename,buf);
			int fd = open(filename,O_WRONLY | O_CREAT ,0666);
			while((ret  = recv(sock,buf,strlen(buf),0))>0)
			{
				printf("%d\n",ret);
				write(fd,buf,ret);
				memset(buf,0,sizeof(buf));
			}
			close(fd);
			printf("write over\n");
		}
	}
}

int main()
{
	int sock = CreateTcpServe(8888);
	if(sock<0)
	{
		return 0;
	}
	struct sockaddr_in sad;
	int len = sizeof(sad);
	pthread_t tid;
	int csock;
	char buf[30]= {0};
	char ip[30]={0};

	while((csock = accept(sock,(struct sockaddr *)&sad,&len))>=0)
	{
		inet_ntop(AF_INET,&sad.sin_addr,ip,sizeof(ip));
		printf("ip:%s\n",ip);
		memset(ip,0,sizeof(ip));
		int *p = (int *)malloc(sizeof(int));
		*p = csock;
		pthread_create(&tid,NULL,fun,p);
	}
}