#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <pthread.h>

void *fun2(void *arg)
{
	int sock = *(int *)arg;
	char buf[1024]={0};
	int ret;
	sleep(1);
	while((ret  = recv(sock,buf,sizeof(buf),0))>0)
	{
		printf("%s",buf);
		memset(buf,0,sizeof(buf));
	}
	exit(0);
}

int main()
{
	int sock = socket(AF_INET,SOCK_STREAM,0);
	struct sockaddr_in ad;
	ad.sin_family = AF_INET;
	ad.sin_port = htons(8888);
	inet_pton(AF_INET,"127.0.0.1",&ad.sin_addr.s_addr);

	if(connect(sock,(struct sockaddr *)&ad,sizeof(ad)))
	{
		printf("connect error\n");
		close(sock);
		return 0;
	}
	pthread_t tid1,tid2;
	int *p = (int *)malloc(sizeof(int));
	*p = sock;
	pthread_create(&tid2,NULL,fun2,p);
	char buf[1024]={0};
	char filename[40]={0};
	int ret;
	struct stat buff;
	while(1)
	{
		printf("input:\n");
		gets(buf);
		send(sock,buf,sizeof(buf),0);
//		write(sock,"list",4);
		if(strcmp(buf,"get")==0)
		{
			memset(buf,0,sizeof(buf));
			gets(buf);
			send(sock,buf,sizeof(buf),0);
			strcpy(filename,"./down/");
			strcat(filename,buf);
			int fd = open(filename,O_WRONLY | O_CREAT ,0666);
			while((ret  = recv(sock,buf,strlen(buf),0))>0)
			{
				write(fd,buf,ret);
				memset(buf,0,sizeof(buf));
			}
			close(fd);
			printf("write cver\n");
		}
		else if(strcmp(buf,"put")==0)
		{
			memset(buf,0,sizeof(buf));
			gets(buf);
			send(sock,buf,sizeof(buf),0);
			int fd = open(buf,O_RDONLY);
			if(fd == 0)
			{
				send(sock,"you input wrong\n",1024,0);
				continue;
			}
			while((ret = read(fd,buf,1024))>0)
			{
				send(sock,buf,ret,0);
				memset(buf,0,sizeof(buf));
			}
			close(fd);
			printf("read over\n");
		}
	}
}