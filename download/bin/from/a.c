#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

void *fun(void *arg)
{
	int sock =*(int *)arg;
	char buf[30]={0};
	int ret;
	while((ret  = read(sock,buf,sizeof(buf)))>0)
	{
		printf("rec[%d]%s\n",ret,buf);
		memset(buf,0,sizeof(buf));
	}
	exit(0);
}

int main()
{
	int sock = socket(AF_INET,SOCK_STREAM,0);
	struct sockaddr_in ad;
	ad.sin_family = AF_INET;
	ad.sin_port = htons(5555);
	inet_pton(AF_INET,"192.168.0.106",&ad.sin_addr.s_addr);

	if(connect(sock,(struct sockaddr *)&ad,sizeof(ad)))
	{
		printf("connect error\n");
		close(sock);
		return 0;
	}
	pthread_t tid;
	pthread_create(&tid,NULL,fun,&sock);

	printf("connect success\n");
	char buf[30];
	while(1)
	{
		gets(buf);
		if(strcmp(buf,"quit")==0)
		{
			break;
		}
		write(sock,buf,strlen(buf));
		memset(buf,0,sizeof(buf));
	}
	close(sock);
	return 0;
}