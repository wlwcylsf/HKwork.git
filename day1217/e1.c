/*有1 2 3 4，能组成多少个互不相同且无重复数的三位数*/

#include <stdio.h>
int main()
{
	int i,j,m,n=0,a,b,c,d;
	for(i=1;i<=4;i++)
	{
		a=i;
		for(j=1;j<=4;j++)
		{
			if(j==a) continue;
			b=j;
			for(m=1;m<=4;m++)
			{
				if(m==a||m==b) continue;
				c=m;
				d=a*100+b*10+c;
				n++;
				printf("%d ",d);
			}
		}
	}
	printf("\n");
	printf("%d\n",n);
}
