题目 贪吃蛇 程序说明

一功能实现

1.贪吃蛇正常的移动，wsad控制上下左右移动。

2.贪吃蛇每吃一个食物增长1格，分数加1.

3.分数每增加5分，食物数量增加一个。食物时间可以为20，30，40.分数随时间变化而变化。

4.空格为暂停,F可以加速5格。P为退出并保存。

5.蛇头碰墙，碰身体，反方向都视为死亡。


二 程序分析

链表头是蛇尾

1.创建链表，蛇的与食物嘚
```
struct foood_node *fcreat_link(int m)
{
	srand(time(NULL));
	struct food_node *p,*ptail;
	int i;
	for(i=0;i<m;i++)
	{
		p = (struct food_node *)malloc(sizeof(struct food_node));
		if(p == NULL)
		{
			free(fhead);
			printf("failure\n");
			return NULL;
		}
		p->date.x = rand()%38 + 21;
		p->date.y = rand()%19 + 6;
		p->date.time = (rand()%3+2)*10;
		if(i == 0)
			fhead = p;
		else
			ptail->next = p;
		ptail = p;
		p->next = NULL;
	}
}

struct snake_node *creat_link(int n)
{
	struct snake_node *p,*ptail;
	int i;
	for(i=0;i<n;i++)
	{
		p = (struct snake_node *)malloc(sizeof(struct snake_node));
		if(p == NULL)
		{
			free(head);
			printf("failure\n");
			return NULL;
		}
		p->date.x = 30+i;
		p->date.y = 15;
		p->date.dir = RIGHT;
		if(i == 0)
			head = p;
		else
			ptail->next = p;
		ptail = p;
		p->next = NULL;
	}
}
```
结构体定义见程序，此处不详解释

2.蛇的移动
```
p = head;
		while(p!= NULL && p->next!=NULL)
		{
			p->date.x = p->next->date.x;
			p->date.y = p->next->date.y;
			p->date.dir = p->next->date.dir;

			p = p->next;
		}
		if(cur_dir == RIGHT || cur_dir == LEFT)
			p->date.x += (cur_dir-3);
		else
			p->date.y += cur_dir;
		p->date.dir = cur_dir;
	
```
将蛇后一个结点的数据赋给前边的，并对最后一个数据判断此时方向对其作相应的操作，上下Y+-1，左右 X+-1;

3.蛇的增长

判断蛇头及链尾的下一次位置（将来时）与食物的坐标进行比较，如果相等，则创建一个结点将 将来时的数据赋予，并将其连到链尾。不相等继续移动（判断时蛇不移动）。
```
pnew = (struct snake_node *)malloc(sizeof(struct snake_node));
		pnew->date.x = q.date.x;
		pnew->date.y = q.date.y;
		pnew->date.dir = cur_dir;
		p = head;
		while(p!=NULL)
		{
			pold = p;
			p = p->next;
		}
		pold->next = pnew;
		pnew->next = p;
```
4.食物的增加

此处采用的是食物链表，所以和蛇的增加相似
```
struct food_node *pnew,*pold,*p = fhead;
	if(score%5 == 0)
	{
		foodlength++;
		pnew = (struct food_node *)malloc(sizeof(struct food_node));
		pnew->date.x = rand()%38 + 21;
		pnew->date.y = rand()%19 + 6;
		p->date.time = (rand()%3+2)*10;
		p = fhead;
		while(p!=NULL)
		{
			pold = p;
			p = p->next;
		}
		pold->next = pnew;
		pnew->next = p;
	}
```
5.数据的保存于读取

1.保存

将版本，状态量，蛇的各个节点数据，食物节点数据，分数，蛇的长度，写入一个文件中，（注意格式）；
```
fp = fopen("snake.dat","w");
	if(fp == NULL)
	{
		perror("open write error");
		return;
	}
	fwrite("SNAKEA",6,1,fp);
	fwrite(&ver,4,1,fp);
	fwrite(&snakelength,4,1,fp);
	fwrite(&score,4,1,fp);
	p = head;
	while(p!= NULL)
	{
		fwrite(&p->date,sizeof(struct snake_date),1,fp);
		p = p->next;
	}
	fwrite(&foodlength,4,1,fp);
	q = fhead;
	while(q != NULL)
	{
		fwrite(&q->date,sizeof(struct food_date),1,fp);
		q = q->next;
	}
	fclose(fp);
```
2.读取要与写时顺序一致，（注意格式）
```
fread(&ver,4,1,fp);
				if(ver == 1)
				{
					int i;
					struct snake_node *p;
					struct snake_date date1;
					struct food_node *q;
					struct food_date date2;	
					fread(&n,4,1,fp);
					snakelength = n;
					fread(&score,4,1,fp);
					creat_link(snakelength);
					p = head;
					for(i=0;i<n;i++)
					{
						fread(&date1,sizeof(struct snake_date),1,fp);
						p->date = date1;
						cur_dir = p->date.dir;
						p = p->next;
					}
					fread(&m,4,1,fp);
					foodlength = m;
					fcreat_link(foodlength);
					q = fhead;
					for(i=0;i<m;i++)
					{
						fread(&date2,sizeof(struct food_date),1,fp);
						q->date = date2;
						q = q->next;
					}
```
6.此处只详解了部分，其余的都比较容易看懂。
编写时要仔细，认真，最好能把大纲列一下。
