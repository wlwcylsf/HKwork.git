#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

extern char **environ;
int main()
{
	setenv("www","www",1);
	pid_t pid = fork();
	if(pid<0)
	{
		printf("fork1 error\n");
		return 0;
	}
	else if(pid == 0)
	{
		for(int i=0;environ != NULL;i++)
			printf("%s\n",environ[i]);
	}
	else
	{
		setenv("bbb","bbb",1);
	}
	getchar();
}