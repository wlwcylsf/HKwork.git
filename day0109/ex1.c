#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
	char *p =(char*)malloc(20);
	pid_t pid = fork();
	if(pid<0)
	{
		printf("fork1 error\n");
		return 0;
	}
	else if(pid == 0)
	{
		printf("world\n");
	}
	else
	{
		strcpy(p,"hello");
		getchar();
	}
}