#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "bill.h"
#include "room.h"
#include "costumer.h"
#include "user.h"


#define COSTUMER_FILE "costumer.txt"

int costumerMenu()
{
	int c;
	printf("\t\t\t    Hetol Management       \n");
	printf("\t\t\t*************************************\n");
	printf("\t\t\t    Administrator System\n");
	printf("\t\t\t    1 : find rooms\n");
	printf("\t\t\t    2 : find self\n");
	printf("\t\t\t    0 : quit\n");
	printf("\t\t\t*************************************\n");
	printf("select:");
	scanf("%d",&c);
	getchar();
	while(c<0 || c>2)
	{
		printf("you input is wrong,please enter again!\n");
		scanf("%d",&c);
		getchar();
	}
	return c;
}

void costumersystem()
{
	Room *pt;
	Costumer *pr;
	int c,loop = 1;
	while(loop)
	{
		system("clear");
		//showTime(1);
		c = costumerMenu();
		switch(c){
			case 1:pt = read_room();
			find_room(pt);
			break;
			case 2:pr = read_costumer();
			find_self(pr);
			break;
			default:
			loop = 0;
		}//switch
	}//while
}

int find_self(Costumer *head)
{
	char id[20];
//	int id;
	int num=1;
	Costumer *p = head;
	printf("please input your m_id\n");
	fgets(id,19,stdin);
//	scanf("%d",&id);
//	getchar();
	while(p!=NULL)
	{
		printf("%d",strncmp(p->m_id,id,6));
		if(strncmp(p->m_id,id,6)==0)
		{
			printf("%-2d,R_ID:%-4d,M_ID:%-24s,NAME:%-10s,AGE:%-4d,SEX:%-4s,PHONE:%-15s,Status:%-4d\n",num++,p->roomId,p->m_id,p->m_name,p->m_age,p->m_sex,p->phone,p->m_status);
			break;
		}
		p=p->next;
	}
	printf("enter any key to continue\n");
	getchar();
	return 0;
}

int write_costumer(Costumer *r)
{
	FILE *fp;
	fp = fopen(COSTUMER_FILE,"a");
	if(fp == NULL)
	{
		perror("open user file");
		return 0;
	}
	memset(fp,0,sizeof(Costumer));
	fprintf(fp,"%-4d",r->roomId);
	fprintf(fp,"%-20s",r->m_id);
	fprintf(fp,"%-9s",r->m_name);
	fprintf(fp,"%-4d",r->m_age);
	fprintf(fp,"%-4s",r->m_sex);
	fprintf(fp,"%-12s",r->phone);
	fprintf(fp,"%-4d",r->m_status);
	fclose(fp);
	return 0;
}

Costumer* read_costumer()
{
	Costumer *head;
	Costumer *p;
	Costumer *pr;
	Costumer *newnode;
	if(access("costumer.txt",F_OK) != 0)
	{
		printf("costumer.txt is NULL\n");
		pr = (Costumer *)malloc(sizeof(Costumer));
		if(pr == NULL){
		return NULL;
		}
		pr->roomId=0;
		strcpy(pr->m_id,"320323199608176810");
		strcpy(pr->m_name,"wanglu");
		pr->m_age = 22;
		strcpy(pr->m_sex,"m");
		strcpy(pr->phone,"15952269362");
		pr->m_status = 1;
		write_costumer(pr);
		return pr;
	}
	else
	{
		FILE *fp;
		fp = fopen(COSTUMER_FILE,"r");
		if(fp == NULL)
		{
			printf("open error");
			return NULL;
		}
		head = (Costumer *)malloc(sizeof(Costumer));
		if(head == NULL){
		printf("malloc error");
		fclose(fp);
		return NULL;
		}
		memset(head,0,sizeof(Costumer));
		head->next = NULL;
		p = head;
//		pold = head;
		while(!feof(fp)){
		fscanf(fp,"%d",&p->roomId);
		fscanf(fp,"%s",p->m_id);
		fscanf(fp,"%s",p->m_name);
		fscanf(fp,"%d",&p->m_age);
		fscanf(fp,"%s",p->m_sex);
		fscanf(fp,"%s",p->phone);
		fscanf(fp,"%d",&p->m_status);
		newnode = (Costumer *)malloc(sizeof(Costumer));
		memset(newnode,0,sizeof(Costumer));
//		pold = p;
		p -> next = newnode;
		p = newnode;
	}
//	free(p);
//	pold->next = NULL;
	fclose(fp);
	return head;
	}
}

int display_costumer(Costumer *head)
{
	Costumer *p;
	int num = 1;
	p = head;
	printf("###############################\n");
	while(p->next->next)
	{
		printf("%-2d,R_ID:%-4d,M_ID:%-24s,NAME:%-10s,AGE:%-4d,SEX:%-4s,PHONE:%-15s,Status:%-4d\n",num++,p->roomId,p->m_id,p->m_name,p->m_age,p->m_sex,p->phone,p->m_status);
		p = p->next;
	}
	printf("###############################\n");
	printf("enter any key to continue\n");
	getchar();
	return 0;
}

int rewrite_costumer(Costumer *head)
{
	Costumer *p;
	p = head;
	FILE *fp;
	fp = fopen(COSTUMER_FILE,"w");
	if(fp == NULL)
	{
		printf("open error");
		return 0;
	}
	fseek(fp,0,SEEK_SET);
	while(p != NULL)
	{
		fprintf(fp,"%-4d",p->roomId);
		fprintf(fp,"%-20s",p->m_id);
		fprintf(fp,"%-9s",p->m_name);
		fprintf(fp,"%-4d",p->m_age);
		fprintf(fp,"%-4s",p->m_sex);
		fprintf(fp,"%-12s",p->phone);
		fprintf(fp,"%-4d",p->m_status);
		p = p->next;
	}
	fclose(fp);
	return 0;
}

int add_costumer(Room *head)
{
	Room *p = head;
	Costumer *pr;
	pr = (Costumer*)malloc(sizeof(Costumer));
	if(pr == NULL){
		return 1;
	}
	printf("please input room Id:\n");
	scanf("%d",&pr->roomId);
	getchar();
	printf("please input m_id\n");
	fgets(pr->m_id,19,stdin);
	printf("please input m_name\n");
	fgets(pr->m_name,8,stdin);
	printf("please input m_age\n");
	scanf("%d",&pr->m_age);
	getchar();
	printf("please input m_sex\n");
	fgets(pr->m_sex,3,stdin);
	printf("please input phone\n");
	fgets(pr->phone,11,stdin);
	pr->m_status = 0;
	write_costumer(pr);

	Bill *qr;
	qr = (Bill *)malloc(sizeof(Bill));
	if(qr == NULL){
		return 1;
	}
	while(p!=NULL)
	{
		if(p->roomId == pr->roomId)
		{
			qr->roomId = p->roomId;
			qr->price = p->price;
			qr->cashpledge = p->cashpledge;
			qr->planprice = p->price;
			qr->realprice = 0;
			qr->change = 0;
			qr->status = 0;
			p->status = 2;
		}
		p = p->next;
	}
	write_bill(qr);
	p = head;
	rewrite_room(p);

	return 0;
}