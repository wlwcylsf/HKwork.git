#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "user.h"

#define USER_FILE "user.txt"

int write_user(User *r)
{
	FILE *fp;
	fp = fopen(USER_FILE,"a");
	if(fp == NULL)
	{
		perror("open user file");
		return 0;
	}
	memset(fp,0,sizeof(User));
	fprintf(fp,"%-12s",r->userId);
	fprintf(fp,"%-7s",r->password);
	fprintf(fp,"%-9s",r->name);
	fprintf(fp,"%-4s",r->sex);
	fprintf(fp,"%-8s",r->role);
	fprintf(fp,"%-20s\n",r->email);
	fclose(fp);
	return 0;
}

User* read_user()
{
	User *head;
	User *p;
	User *newnode;
	FILE *fp;
	fp = fopen(USER_FILE,"r");
	if(fp == NULL)
	{
		printf("open error");
		return NULL;
	}
	head = (User *)malloc(sizeof(User));
	if(head == NULL){
		printf("malloc error");
		fclose(fp);
		return NULL;
	}
	memset(head,0,sizeof(User));
	head->next = NULL;
	p = head;
	while(!feof(fp)){
		fscanf(fp,"%s",p->userId);
		fscanf(fp,"%s",p->password);
		fscanf(fp,"%s",p->name);
		fscanf(fp,"%s",p->sex);
		fscanf(fp,"%s",p->role);
		fscanf(fp,"%s",p->email);
		newnode = (User *)malloc(sizeof(User));
		memset(newnode,0,sizeof(User));
		p -> next = newnode;
		p = newnode;
	}
	free(p);
	fclose(fp);
	return head;
}

int display_user(User *head)
{
	User *p;
	int num = 1;
	p = head;
	printf("###############################\n");
	while(p->next)
	{
		printf("%-2d,ID:%-12s,Password:%-8s,Name:%-10s,Sex:%-4s,Role:%-10s,Email:%-15s\n",num++,p->userId,p->password,p->name,p->sex,p->role,p->email);
		p = p->next;
	}
	printf("###############################\n");
	printf("enter any key to continue\n");
	getchar();
	return 0;
}

int rewrite_user(User *head)
{
	User *p;
	p = head;
	FILE *fp;
	fp = fopen(USER_FILE,"w");
	if(fp == NULL)
	{
		printf("open error");
		return 0;
	}
	fseek(fp,0,SEEK_SET);
	while(p != NULL)
	{
		fprintf(fp,"%-12s",p->userId);
		fprintf(fp,"%-7s",p->password);
		fprintf(fp,"%-9s",p->name);
		fprintf(fp,"%-4s",p->sex);
		fprintf(fp,"%-8s",p->role);
		fprintf(fp,"%-20s\n",p->email);
		p = p->next;
	}
	fclose(fp);
	return 0;
}

int add_user()
{
	User *pr;
	char *role_str[] = {"admin","manager","recept","cleaner"};
	pr = (User*)malloc(sizeof(User));
		if(pr == NULL){
		return 1;
		}
		getchar();
		printf("please input user Id:\n");
		fgets(pr->userId,11,stdin);
		printf("please input user password:\n");
		fgets(pr->password,6,stdin);
//		printf("%s\n",pr->password);
		printf("please input user role:\n");
		fgets(pr->name,8,stdin);
		int s;
		printf("role  1:admin,2:manager,3:recept,4:cleaner");
		scanf("%d",&s);
		while(s<1 || s>4){
			printf("enter wrong! enter again!\n");
			scanf("%d",&s);
		}
		strcpy(pr->role,role_str[s-1]);
		getchar();
		printf("please input user name:\n");
		fgets(pr->name,8,stdin);
		printf("please input user sex:\n");
		fgets(pr->sex,3,stdin);
		printf("please input user email:\n");
		fgets(pr->email,19,stdin);
		write_user(pr);
		return 0;
}

int remove_user(User *head)
{
	User *p;
	User *pold;
	int ID,num = 1;
	printf("please input you want to remove the ID:\n");
	scanf("%d",&ID);
	while(ID<2)
	{
		printf("you have no right to remove\n");
		return 0;	
	}
	p = head;
	pold = head;
	while(p != NULL)
	{
//		printf("%d %d ",ID,num);
		if(num == ID)
		{
			pold->next = p->next;
			free(p);
			break;
		}
		else
		{
			pold = p;
			p = p->next;
		}
		num++;
	}
	p = head;
	rewrite_user(p);
	getchar();
	printf("enter any key to continue\n");
	getchar();
	return 0;
}

int setup_password(User *head)
{
	User *p;
	p = head;
	p = p->next;
	while(p->next!=NULL)
	{
		strcpy(p->password,"123456");
		p = p->next;
	}
	p = head;
	rewrite_user(p);
	getchar();
	printf("enter any key to continue\n");
	getchar();
	return 0;
}

int change_user(User *head)
{
	User *p;
	char userid[12],password[7],name[9],sex[4],email[20];
	char *role_str[] = {"admin","manager","recept","cleaner"};
	p = head;
	int i,j,s;
	printf("enter which one person you want to change:\n");
	scanf("%d",&i);
	for(int num = 1;p!=NULL&&num!=i;num++)
		p = p->next;
	printf("enter which information you want to change:\n");
	printf("1.userId 2.password 3.name 4.sex 5.role 6.email\n");
	scanf("%d",&j);
	while(j<0 || j>6)
	{
		printf("you input is wrong,please enter again!\n");
		scanf("%d",&j);
	}

	switch(j){
		case 1:
		printf("please input userId:\n");
		scanf("%s",userid);
		strcpy(p->userId,userid);
		printf("change success\n");
		break;
		case 2:
		printf("please input user password:\n");
		scanf("%s",password);
		strcpy(p->password,password);
		printf("change success\n");
		break;
		case 3:
		printf("please input user name:\n");
		scanf("%s",name);
		strcpy(p->name,name);
		printf("change success\n");
		break;
		case 4:
		printf("please input user sex:\n");
		scanf("%s",sex);
		strcpy(p->sex,sex);
		printf("change success\n");
		break;
		case 5:
		printf("role  1:admin,2:manager,3:recept,4:cleaner");
		scanf("%d",&s);
		while(s<1 || s>4){
			printf("enter wrong! enter again!\n");
			scanf("%d",&s);
		}
		strcpy(p->role,role_str[s-1]);
		break;
		case 6:
		printf("please input user email:\n");
		scanf("%s",email);
		strcpy(p->email,email);
		printf("change success\n");
		break;
	}
	p = head;
	rewrite_user(p);
	getchar();
	printf("enter any key to continue\n");
	getchar();
	return 0;
}