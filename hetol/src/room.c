#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "room.h"
#include "costumer.h"
#include "bill.h"

#define ROOM_FILE "room.txt"

int write_room(Room *r)
{
	FILE *fp;
	fp = fopen(ROOM_FILE,"a");
	if(fp == NULL)
	{
		perror("open user file");
		return 0;
	}
	memset(fp,0,sizeof(Room));
	fprintf(fp,"%-4d",r->roomId);
	fprintf(fp,"%-8s",r->kind);
	fprintf(fp,"%-4d",r->price);
	fprintf(fp,"%-4d",r->discount);
	fprintf(fp,"%-6d",r->cashpledge);
	fprintf(fp,"%-4d\n",r->status);
	fclose(fp);
	return 0;
}

Room* read_room()
{
	Room *head;
	Room *p;
	Room *newnode;
	Room *pr;
//	Room *pold;
	if(access("room.txt",F_OK) != 0)
	{
		printf("room.txt is NULL\n");
		pr = (Room*)malloc(sizeof(Room));
		if(pr == NULL){
		return NULL;
		}
		pr->roomId=0;
		strcpy(pr->kind,"single");
		pr->price=100;
		pr->discount=0;
		pr->cashpledge=100;
		pr->status = 3;
		write_room(pr);
		return pr;
	}
	else
	{
		FILE *fp;
		fp = fopen(ROOM_FILE,"r");
		if(fp == NULL)
		{
			printf("open error");
			return NULL;
		}
		head = (Room *)malloc(sizeof(Room));
		if(head == NULL){
		printf("malloc error");
		fclose(fp);
		return NULL;
		}
		memset(head,0,sizeof(Room));
		head->next = NULL;
		p = head;
//		pold = head;
		while(!feof(fp)){
		fscanf(fp,"%d",&p->roomId);
		fscanf(fp,"%s",p->kind);
		fscanf(fp,"%d",&p->price);
		fscanf(fp,"%d",&p->discount);
		fscanf(fp,"%d",&p->cashpledge);
		fscanf(fp,"%d",&p->status);
		newnode = (Room *)malloc(sizeof(Room));
		memset(newnode,0,sizeof(Room));
//		pold = p;
		p -> next = newnode;
		p = newnode;
	}
//	free(p);
//	pold->next = NULL;
	fclose(fp);
	return head;
	}
}

int display_room(Room *head)
{
	Room *p;
	int num = 1;
	p = head;
	printf("###############################\n");
	while(p->next->next)
	{
		printf("%-2d,ID:%-4d,Kind:%-8s,Price:%-6d,Discount:%-4d,Cashpledge:%-6d,Status:%-4d\n",num++,p->roomId,p->kind,p->price,p->discount,p->cashpledge,p->status);
		p = p->next;
	}
	printf("###############################\n");
	printf("enter any key to continue\n");
	getchar();
	return 0;
}

int rewrite_room(Room *head)
{
	Room *p;
	p = head;
	FILE *fp;
	fp = fopen(ROOM_FILE,"w");
	if(fp == NULL)
	{
		printf("open error");
		return 0;
	}
	fseek(fp,0,SEEK_SET);
	while(p != NULL)
	{
		fprintf(fp,"%-4d",p->roomId);
		fprintf(fp,"%-8s",p->kind);
		fprintf(fp,"%-4d",p->price);
		fprintf(fp,"%-4d",p->discount);
		fprintf(fp,"%-6d",p->cashpledge);
		fprintf(fp,"%-4d\n",p->status);
		p = p->next;
	}
	fclose(fp);
	return 0;
}

int add_room()
{
	Room *pr;
	int num;
	char *kind_str[] = {"single","double","three"};
	pr = (Room*)malloc(sizeof(Room));
		if(pr == NULL){
		return 1;
		}
		getchar();
		printf("please input room Id:\n");
		scanf("%d",&pr->roomId);
		int s;
		printf("kind   1:single,2:double,3:three\n");
		scanf("%d",&s);
		while(s<1 || s>3)
		{
			printf("enter wrong! enter again!\n");
			scanf("%d",&s);
		}
		strcpy(pr->kind,kind_str[s-1]);
		getchar();
		printf("please input price:\n");
		scanf("%d",&pr->price);
		printf("please input discount:\n");
		scanf("%d",&pr->discount);
		printf("please input cashpledge:\n");
		scanf("%d",&pr->cashpledge);
		printf("please input status:\n");
		printf("status 1:empty,2:live\n");
		scanf("%d",&num);
		while(num<1 || num >2)
		{
			printf("enter wrong! enter again\n");
			scanf("%d",&num);
		}
		pr->status = num;
		write_room(pr);
		return 0;
}

int remove_room(Room *head)
{
	Room *p;
	Room *pold;
	int ID,num = 1;
	printf("please input you want to remove the ID:\n");
	scanf("%d",&ID);
	p = head;
	pold = head;
	while(p != NULL)
	{
//		printf("%d %d ",ID,num);
		if(num == ID)
		{
			pold->next = p->next;
			free(p);
			break;
		}
		else
		{
			pold = p;
			p = p->next;
		}
		num++;
	}
	p = head;
	rewrite_room(p);
	getchar();
	printf("enter any key to continue\n");
	getchar();
	return 0;
}

int change_room(Room *head)
{
	Room *p;
	char *kind_str[] = {"single","double","three"};
	p = head;
	int i,j,s;
	printf("enter which one person you want to change:\n");
	scanf("%d",&i);
	for(int num = 1;p!=NULL&&num!=i;num++)
		p = p->next;
	printf("enter which information you want to change:\n");
	printf("1.roomId 2.kind 3.price 4.discount 5.cashledge 6.status\n");
	scanf("%d",&j);
	while(j<0 || j>6)
	{
		printf("you input is wrong,please enter again!\n");
		scanf("%d",&j);
	}

	switch(j){
		case 1:
		printf("please input userId:\n");
		scanf("%d",&p->roomId);
		printf("change success\n");
		break;
		case 2:
		printf("kind  1:single,2:double,3:three\n");
		scanf("%d",&s);
		while(s<1 || s>3){
			printf("enter wrong! enter again!\n");
			scanf("%d",&s);
		}
		strcpy(p->kind,kind_str[s-1]);
		printf("change success\n");
		break;
		case 3:
		printf("please input price:\n");
		scanf("%d",&p->price);
		printf("change success\n");
		break;
		case 4:
		printf("please input discount:\n");
		scanf("%d",&p->discount);
		printf("change success\n");
		break;
		case 5:
		printf("please input cashpledge:\n");
		scanf("%d",&p->cashpledge);
		printf("change success\n");
		break;
		case 6:
		printf("please input Status:\n");
		scanf("%d",&p->status);
		printf("change success\n");
		break;
	}
	p = head;
	rewrite_room(p);
	getchar();
	printf("enter any key to continue\n");
	getchar();
	return 0;
}

int find_room(Room* head)
{
	Room *p = head;
	int s,num=1;
	char str[10];
	char *kind_str[] = {"single","double","three","empty","discount"};
	printf("which kind would you find\n");
	printf("kind  1:single,2:double,3:three,4:empty,5:dicount\n");
	scanf("%d",&s);
	while(s<1 || s>5){
		printf("enter wrong! enter again!\n");
		scanf("%d",&s);
	}
	strcpy(str,kind_str[s-1]);
	switch(s)
	{
		case 1:
		case 2:
		case 3:
		while(p!=NULL)
		{
			if(strcmp(str,p->kind)==0)
				printf("%-2d,ID:%-4d,Kind:%-8s,Price:%-6d,Discount:%-4d,Cashpledge:%-6d,Status:%-4d\n",num++,p->roomId,p->kind,p->price,p->discount,p->cashpledge,p->status);
			p = p->next;
		}
		break;
		case 4:
		while(p!=NULL)
		{
			if(p->status == 1)
				printf("%-2d,ID:%-4d,Kind:%-8s,Price:%-6d,Discount:%-4d,Cashpledge:%-6d,Status:%-4d\n",num++,p->roomId,p->kind,p->price,p->discount,p->cashpledge,p->status);
			p = p->next;
		}
		break;
		case 5:
		while(p!=NULL)
		{
			if(p->discount == 1)
				printf("%-2d,ID:%-4d,Kind:%-8s,Price:%-6d,Discount:%-4d,Cashpledge:%-6d,Status:%-4d\n",num++,p->roomId,p->kind,p->price,p->discount,p->cashpledge,p->status);
			p = p->next;
		}
		break;
	}
	getchar();
	printf("enter any key to continue\n");
	getchar();
	return 0;
}

int rechange_room(Room *Rhead,Bill *Bhead)
{
	Room *pa = Rhead;
	Bill *pc = Bhead;
	Costumer *Chead = read_costumer();
	Costumer *pb = Chead;
	int oldid,newid;
	printf("please input your origin roomid\n");
	scanf("%d",&oldid);
	getchar();
	printf("please input your new roomid\n");
	scanf("%d",&newid);
	getchar();

	while(pa!=NULL)
	{
		if(pa->roomId == oldid)
		{
			pa->status = 3;
		}
		pa = pa->next;
	}
	pa = Rhead;
	rewrite_room(pa);

	while(pb!=NULL)
	{
		if(pb->roomId == oldid)
		{
			pb->roomId = newid;
		}
		pb = pb->next;
	}
	pb = Chead;
	rewrite_costumer(pb);

	while(pc!=NULL)
	{
		if(pc->roomId==oldid)
		{

			pa = Rhead;
			while(pa!=NULL)
			{
				if(newid == pa->roomId)
				{
					pc->roomId = pa->roomId;
					pc->price = pa->price;
					pc->cashpledge = pa->cashpledge;
					pc->planprice = pa->price;
					pc->realprice = 0;
					pc->change = 0;
					pc->status = 0;
					pa->status = 2;
				}
				pa = pa->next;
			}
		}
		pc = pc->next;
	}
	pc = Bhead;
	rewrite_bill(pc);
	printf("enter any key to continue\n");
	getchar();
	return 0;
}

int out_room()
{
	int roomid;
	Room *pa,*Rhead;
	Bill *pc,*Bhead;
	Rhead = read_room();
	Bhead = read_bill();
	pa = Rhead;
	pc = Bhead;
	printf("please input the roomid of out room\n");
	scanf("%d",&roomid);
	getchar();
	while(pa!=NULL)
	{
		if(pa->roomId == roomid)
		{
			pa->status = 1;
		}
		pa = pa->next;
	}
	pa = Rhead;
	rewrite_room(pa);

	while(pc!=NULL)
	{
		if(pc->roomId == roomid)
		{
			remove_bill(roomid);
			break;
		}
	}
	printf("enter any key to continue\n");
	getchar();
	return 0;
}

int noclean_room(Room *head)
{
	Room *p = head;
	int num=1;
	while(p!=NULL)
	{
		if(p->status==3)
			printf("%-2d,ID:%-4d,Kind:%-8s,Price:%-6d,Discount:%-4d,Cashpledge:%-6d,Status:%-4d\n",num++,p->roomId,p->kind,p->price,p->discount,p->cashpledge,p->status);
		p = p->next;
	}
	printf("enter any key to continue\n");
	getchar();
	return 0;
}

int clean_ok(Room *head)
{
	int id,num = 1;
	Room *p = head;
	printf("please input the id of room\n");
	scanf("%d",&id);
	getchar();
	while(p!=NULL)
	{
		if(p->roomId == id)
		{
			p->status = 1;
			printf("%-2d,ID:%-4d,Kind:%-8s,Price:%-6d,Discount:%-4d,Cashpledge:%-6d,Status:%-4d\n",num++,p->roomId,p->kind,p->price,p->discount,p->cashpledge,p->status);
			break;		
		}
		p=p->next;
	}
	printf("enter any key to continue\n");
	getchar();
	return 0;
}