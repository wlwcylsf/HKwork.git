#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "user.h"

int adminMenu()
{
	int c;
	printf("\t\t\t    Hetol Management       \n");
	printf("\t\t\t*************************************\n");
	printf("\t\t\t    Administrator System\n");
	printf("\t\t\t    1 : show user\n");
	printf("\t\t\t    2 : add user\n");
	printf("\t\t\t    3 : remove user\n");
	printf("\t\t\t    4 : setup user password\n");
	printf("\t\t\t    5 : change user\n");
	printf("\t\t\t    6 : backup user\n");
	printf("\t\t\t    0 : quit\n");
	printf("\t\t\t*************************************\n");
	printf("select:");
	scanf("%d",&c);
	getchar();
	while(c<0 || c>6)
	{
		printf("you input is wrong,please enter again!\n");
		scanf("%d",&c);
		getchar();
	}
	return c;
}

void adminsystem(User *r)
{
	int c,loop = 1;
	User *pr;
	while(loop)
	{
		system("clear");
		//showTime(1);
		printf("%s\n",r->userId);
		c = adminMenu();
		switch(c){
			case 1:pr = read_user();
			display_user(pr);
			break;
			case 2:add_user();
			break;
			case 3:pr = read_user();
			remove_user(pr);
			break;
			case 4:pr = read_user();
			setup_password(pr);
			break;
			case 5:pr = read_user();
			change_user(pr);
			break;
			case 6:break;
			default:
			loop = 0;
		}//switch
	}//while
}