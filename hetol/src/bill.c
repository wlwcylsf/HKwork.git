#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "room.h"
#include "costumer.h"
#include "bill.h"

#define BILL_FILE "bill.txt"

int write_bill(Bill *r)
{
	FILE *fp;
	fp = fopen(BILL_FILE,"a");
	if(fp == NULL)
	{
		perror("open user file");
		return 0;
	}
	memset(fp,0,sizeof(Bill));
	fprintf(fp,"%-4d",r->roomId);
	fprintf(fp,"%-4d",r->price);
	fprintf(fp,"%-4d",r->cashpledge);
	fprintf(fp,"%-4d",r->planprice);
	fprintf(fp,"%-4d",r->realprice);
	fprintf(fp,"%-4d",r->change);
	fprintf(fp,"%-4d\n",r->status);
	fclose(fp);
	return 0;
}

Bill * read_bill()
{
	Bill *head;
	Bill *p;
	Bill *pr;
	Bill *newnode;
	if(access("bill.txt",F_OK) != 0)
	{
		printf("bill.txt is NULL\n");
		pr = (Bill *)malloc(sizeof(Bill));
		if(pr == NULL){
		return NULL;
		}
		pr->roomId=0;
		pr->cashpledge=100;
		pr->price=100;
		pr->planprice=100;
		pr->realprice=100;
		pr->change=0;
		pr->status = 1;
		write_bill(pr);
		return pr;
	}
	else
	{
		FILE *fp;
		fp = fopen(BILL_FILE,"r");
		if(fp == NULL)
		{
			printf("open error");
			return NULL;
		}
		head = (Bill *)malloc(sizeof(Bill));
		if(head == NULL){
		printf("malloc error");
		fclose(fp);
		return NULL;
		}
		memset(head,0,sizeof(Bill));
		head->next = NULL;
		p = head;
//		pold = head;
		while(!feof(fp)){
		fscanf(fp,"%d",&p->roomId);
		fscanf(fp,"%d",&p->price);
		fscanf(fp,"%d",&p->cashpledge);
		fscanf(fp,"%d",&p->planprice);
		fscanf(fp,"%d",&p->realprice);
		fscanf(fp,"%d",&p->change);
		fscanf(fp,"%d",&p->status);
		newnode = (Bill *)malloc(sizeof(Bill));
		memset(newnode,0,sizeof(Bill));
//		pold = p;
		p -> next = newnode;
		p = newnode;
	}
//	free(p);
//	pold->next = NULL;
	fclose(fp);
	return head;
	}
}

int display_bill(Bill *head)
{
	Bill *p;
	int num = 1;
	p = head;
	printf("###############################\n");
	while(p->next->next)
	{
		printf("%-2d,R_ID:%-4d,Price:%-4d,Cashpledge:%-4dPlanprice:%-4d,Realprice:%-4d,Change:%-4d,Status:%-4d\n",num++,p->roomId,p->price,p->cashpledge,p->planprice,p->realprice,p->change,p->status);
		p = p->next;
	}
	printf("###############################\n");
	printf("enter any key to continue\n");
	getchar();
	return 0;
}

int rewrite_bill(Bill *head)
{
	Bill *p;
	p = head;
	FILE *fp;
	fp = fopen(BILL_FILE,"w");
	if(fp == NULL)
	{
		printf("open error");
		return 0;
	}
	fseek(fp,0,SEEK_SET);
	while(p != NULL)
	{
	fprintf(fp,"%-4d",p->roomId);
	fprintf(fp,"%-4d",p->price);
	fprintf(fp,"%-4d",p->cashpledge);
	fprintf(fp,"%-4d",p->planprice);
	fprintf(fp,"%-4d",p->realprice);
	fprintf(fp,"%-4d",p->change);
	fprintf(fp,"%-4d\n",p->status);
		p = p->next;
	}
	fclose(fp);
	return 0;
}

int remove_bill(int ID)
{
	Bill *head = read_bill();
	Bill *p;
	Bill *pold;
	int num = 1;
	p = head;
	pold = head;
	while(p != NULL)
	{
//		printf("%d %d ",ID,num);
		if(p->roomId == ID)
		{
			pold->next = p->next;
			free(p);
			break;
		}
		else
		{
			pold = p;
			p = p->next;
		}
		num++;
	}
	p = head;
	rewrite_bill(p);
	printf("enter any key to continue\n");
	getchar();
	return 0;
}

int check_bill()
{
	int roomid,realprice;
	Room *pa,*Rhead;
	Costumer *pb,*Chead;
	Bill *pc,*Bhead;
	Rhead = read_room();
	Chead = read_costumer();
	Bhead = read_bill();
	pa = Rhead;
	pb = Chead;
	pc = Bhead;
	printf("please input the roomId\n");
	scanf("%d",&roomid);
	getchar();
	printf("pleaes input the realprice\n");
	scanf("%d",&realprice);
	getchar();
	while(pa!=NULL)
	{
		if(pa->roomId == roomid)
		{
			pa->status = 3;
		}
		pa = pa->next;
	}
	pa = Rhead;
	rewrite_room(pa);

	while(pb!=NULL)
	{
		if(pb->roomId == roomid)
		{
			pb->m_status = 1;
		}
		pb = pb->next;
	}
	pb = Chead;
	rewrite_costumer(pb);

	while(pc!=NULL)
	{
		if(pc->roomId==roomid)
		{
			pc->realprice = realprice;
			pc->change = realprice-pc->planprice;
			pc->status = 1;
		}
		pc = pc->next;
	}
	pc = Bhead;
	rewrite_bill(pc);
	return 0;
}