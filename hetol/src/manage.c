#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "user.h"
#include "room.h"

int managerMenu()
{
	int c;
	printf("\t\t\t    Hetol Management       \n");
	printf("\t\t\t*************************************\n");
	printf("\t\t\t    Administrator System\n");
	printf("\t\t\t    1 : show user\n");
	printf("\t\t\t    2 : add user\n");
	printf("\t\t\t    3 : remove user\n");
	printf("\t\t\t    4 : show rooms\n");
	printf("\t\t\t    5 : add rooms\n");
	printf("\t\t\t    6 : remove rooms\n");
	printf("\t\t\t    7 : change rooms\n");
	printf("\t\t\t    8 : show bills\n");
	printf("\t\t\t    0 : quit\n");
	printf("\t\t\t*************************************\n");
	printf("select:");
	scanf("%d",&c);
	getchar();
	while(c<0 || c>10)
	{
		printf("you input is wrong,please enter again!\n");
		scanf("%d",&c);
		getchar();
	}
	return c;
}

void managersystem(User *r)
{
	int c,loop = 1;
	User *pr;
	Room *pt;
	while(loop)
	{
		system("clear");
		//showTime(1);
		printf("%s\n",r->userId);
		c = managerMenu();
		switch(c){
			case 1:pr = read_user();
			display_user(pr);
			break;
			case 2:add_user();
			break;
			case 3:pr = read_user();
			remove_user(pr);
			break;
			case 4:pt = read_room();
			display_room(pt);
			break;
			case 5:add_room();
			break;
			case 6:pt = read_room();
			remove_room(pt);
			break;
			case 7:pt = read_room();
			change_room(pt);
			default:
			loop = 0;
		}//switch
	}//while
}