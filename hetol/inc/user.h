#ifndef _USER_H_
#define _USER_H_

struct usernode
{
	char userId[12];
	char password[7];
	char name[9];
	char sex[4];
	char role[8];//"admin","manager","recept","cleaner";
	char email[20];
	struct usernode *next;
};

typedef struct usernode User;

/*
read out all user information from user.txt to user list;
return 
sucess:return list head;
failure:NULL;
*/
User *read_user();

/*
save one node user information to user.txt;
parameter
User* r:user information struct point
return:
sucess:0
failure:1*/
int write_user(User* node);

/*
read out all user information to user.txt;
parameter:
User* head:user link head
*/
int backup_user(User* head);

/*
free all user information;
parameter:
User* head:user link head
*/
int free_user(User *head);

void adminsystem();
void managersystem();
void receptsystem();
void cleanersystem();
void costumersystem();

int display_user(User *head);
int add_user();
int remove_user(User *head);
int setup_password(User *head);
int change_user(User *head);
int rewrite_user(User *head);

#endif
