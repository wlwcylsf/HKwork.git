#ifndef _COSTUMER_H_
#define _COSTUMER_H_

struct costumnode
{
	int roomId;
	char m_id[20];
	char m_name[9];
	int m_age;
	char m_sex[4];
	char phone[12];
//	char inDay[20];
//	char outDay[20];
	int m_status;
	struct costumnode *next;
};

typedef struct costumnode Costumer;

int find_self(Costumer *head);

int add_costumer(Room *head);

int write_costumer(Costumer *r);

Costumer* read_costumer();

int display_costumer(Costumer *head);

int rewrite_costumer(Costumer *head);

#endif