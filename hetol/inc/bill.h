#ifndef _BILL_H_
#define _BILL_H_

struct billnode
{
	int roomId;
	int price;
	int cashpledge;
	int planprice;
	int realprice;
	int change;
	int status;//(0,weijie 1 yijie)
//	char inDay[20];
//	char outDay[20];
	struct billnode *next;
};
typedef struct billnode Bill;

int write_bill(Bill *r);

Bill * read_bill();

int display_bill(Bill *head);

int rewrite_bill(Bill *head);

int remove_bill(int ID);

int check_bill();

#endif