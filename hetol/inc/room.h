#ifndef _ROOM_H_
#define _ROOM_H_


struct roomnode
{
	int roomId;
	char kind[10];
	int price;
	int discount;//0 normal,1 discount;
	int cashpledge;
	int status;//1 empty,2 have,3 nocleaner.
	struct roomnode *next;
};

typedef struct roomnode Room;

Room *read_room();
/*
read all information from all room.txt to room list;
return
success:room list hesd pointer
failure:NULL
*/
int add_room();

int display_room(Room* head);

int rewrite_room(Room* head);

int write_room(Room* head);

int remove_room(Room* head);

int backup_room(Room* head);

int keep_room(Room* head);

int change_room(Room* head);

int rechange_room();

int out_room();

int find_room(Room* head);

int show_room();

int display_room(Room *head);

int noclean_room(Room *head);

int clean_ok(Room *head);

#endif