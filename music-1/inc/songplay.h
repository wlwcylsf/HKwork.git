#ifndef _SONGPLAY_H_
#define _SONGPLAY_H_

int song_play(Song *p);

void send_message(char *buf);

//Song *judge(char *p);

Song *play_song();

Song *play_next(Song *p);

Song *play_last(Song *p);

void *read_message(void *arg);

void *get_message(void *arg);

void *send_songword(void *arg);

#endif