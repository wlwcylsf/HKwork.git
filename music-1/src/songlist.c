#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>

#include "songlist.h"

#define SONG_FILE "song.txt"

//int NUM = 1;

int write_song(Song *r)
{
	FILE *fp;
	fp = fopen(SONG_FILE,"a");
	if(fp == NULL)
	{
		perror("open song file");
		return 0;
	}
	memset(fp,0,sizeof(Song));
	fprintf(fp,"%-4d",r->num);
	fprintf(fp,"%-19s",r->name);
	fprintf(fp,"%-39s\n",r->path);
	fclose(fp);
	return 0;
}

Song* read_song()
{
	Song *head;
	Song *p;
	Song *newnode;
	FILE *fp;
	fp = fopen(SONG_FILE,"r");
	if(fp == NULL)
	{
		printf("open error");
		return NULL;
	}
	head = (Song *)malloc(sizeof(Song));
	if(head == NULL){
		printf("malloc error");
		fclose(fp);
		return NULL;
	}
	memset(head,0,sizeof(Song));
	head->next = NULL;
	p = head;
	while(!feof(fp)){
		fscanf(fp,"%d",&p->num);
		fscanf(fp,"%s",p->name);
		fscanf(fp,"%s",p->path);
		newnode = (Song *)malloc(sizeof(Song));
		memset(newnode,0,sizeof(Song));
		p -> next = newnode;
		p = newnode;
	}
	p = NULL;
	free(p);
	fclose(fp);
	return head;
}


int display_song(Song *head)
{
	Song *p;
	int num = 1;
	p = head;
	printf("###############################\n");
	while(p->next->next)
	{
		printf("%-2d,NUM:%-4d,NAME:%-20s,PATH:%-40s\n",num++,p->num,p->name,p->path);
		p = p->next;
	}
	printf("###############################\n");
	printf("enter any key to continue\n");
	getchar();
	return 0;
}

int rewrite_song(Song *head)
{
	Song *p;
	p = head;
	FILE *fp;
	fp = fopen(SONG_FILE,"w");
	if(fp == NULL)
	{
		printf("open error");
		return 0;
	}
	fseek(fp,0,SEEK_SET);
	while(p != NULL)
	{
		fprintf(fp,"%-4d",p->num);
		fprintf(fp,"%-19s",p->name);
		fprintf(fp,"%-39s\n",p->path);
		p = p->next;
	}
	fclose(fp);
	return 0;
}

int add_song(Song *head)
{
	Song *p;
	Song *pr = head;
	p = (Song*)malloc(sizeof(Song));
	if(p == NULL){
		return 1;
	}
	printf("please input the num of song:\n");
	scanf("%d",&p->num);
	getchar();
	printf("please input the name of song:\n");
	gets(p->name);
	printf("%s\n",p->name);
	printf("please input the path of song:\n");
	gets(p->path);
	printf("%s\n",p->path);

	if(access(p->path,F_OK ) != 0)
	{
		printf("the song is not existence!\n");
		printf("enter any key to continue\n");
		getchar();
		return 0;
	}

	while(pr!=NULL)
	{
		if(strcmp(p->path,pr->path)==0)
		{
			printf("the song is same with another!\n");
			printf("enter any key to continue\n");
			getchar();
			return 0;
		}
		else
			pr = pr->next;
	}

	write_song(p);
	printf("enter any key to continue\n");
	getchar();
	return 0;
}

int remove_song(Song *head)
{
	Song *p;
	Song *pold;
	int ID,num = 1;
	printf("please input you want to remove the ID:\n");
	scanf("%d",&ID);
	p = head;
	pold = head;
	while(p != NULL)
	{
		if(num == ID)
		{
			pold->next = p->next;
			free(p);
			break;
		}
		else
		{
			pold = p;
			p = p->next;
		}
		num++;
	}
	p = head;
	while(p!=NULL)
	{
		if(p->next == NULL)
		{
			pold->next = p->next;
			free(p);
			break;
		}
		else
		{
			pold = p;
			p = p->next;
		}
	}
	p = head;
	rewrite_song(p);
	printf("enter any key to continue\n");
	getchar();
	return 0;
}



int readfile(char *path,Song *head)
{
char apath[100];
	int n = 1;

	Song *p;
	Song *pr = head;
	p = (Song*)malloc(sizeof(Song));
	if(p == NULL){
		return 1;
	}

	DIR *dir;
	struct dirent *ptr;
	struct stat buf;
	dir = opendir(path);
	printf("%s\n",path);
	while((ptr = readdir(dir)))
	{
		stat(ptr->d_name,&buf);
		if(ptr->d_type == 4 && strncmp(ptr->d_name,"..",2) !=0 && strncmp(ptr->d_name,".",1) !=0)
		{
			strcpy(apath,path);
			strcat(apath,"/");
			strcat(apath,ptr->d_name);
			readfile(apath,head);
//			printf("%10s%10lu  %s\n",ptr->d_name,buf.st_size,ctime(&buf.st_mtime));
		}
		else if(strncmp(ptr->d_name,"..",2) !=0 && strncmp(ptr->d_name,".",1) !=0)
		{
			memset(p,0,sizeof(Song));
			p->num = n;
			n++;
			strcpy(p->name,ptr->d_name);
			strcpy(apath,path);
			strcat(apath,"/");
			strcat(apath,ptr->d_name);
			strcpy(p->path,apath);

			while(pr!=NULL)
			{
					if(strcmp(p->path,pr->path)==0)
				{
					printf("the song is same with another!\n");
					printf("enter any key to continue\n");
					getchar();
					break;
				}
				else
					pr = pr->next;
			}
			pr = head;
			write_song(p);
//			printf("%10s%10lu  %s\n",ptr->d_name,buf.st_size,ctime(&buf.st_mtime));
		}
	}
	printf("enter any key to continue\n");
	getchar();
	return 0;
	}

int add_songfile(Song *head)
{
//	Song *pr = head;
	char path[100];
	printf("please input the path of file:\n");
	gets(path);
	if(access(path,F_OK ) != 0)
	{
		printf("the file is not existence!\n");
		printf("enter any key to continue\n");
		getchar();
		return 0;
	}
	readfile(path,head);
	return 0;
}