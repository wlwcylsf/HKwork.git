#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "songlist.h"
#include "songplay.h"

int fd[2];
int i=1;
char songlength[30];
char schedule[30];
char play_time[15];
int mplayer_pid;
int my_lock = 0;
Song *pn;
float t;

void *read_message(void *arg)
{
	int ret;
	int minnut;
	float sec = 0.0;
	float s1,s2;
	char *b1 = NULL,*b2 = NULL;
	char buf[100];
	while((ret = read(fd[0],buf,sizeof(buf)))>0)
	{
		buf[ret]='\0';
		if(!strncmp(buf,"ANS_LENGTH",10))
		{
			strcat(songlength,buf);
			b1 = songlength;
			b1 = b1 + 11;
			sscanf(b1,"%f",&s1);
			memset(songlength,0,sizeof(songlength));
		}
		else if(!strncmp(buf,"ANS_TIME_POSITION",17))
		{
			strcat(schedule,buf);
			b2 = schedule;
			b2 = b2 + 18;
			sscanf(b2,"%f",&s2);
			t = s2;
			memset(schedule,0,sizeof(schedule));
		}
		else
		{
			printf("%s",buf);
			memset(buf,0,sizeof(buf));
		}
		minnut = (int)s2 / 60;
		sec = s2;
		while((sec - 60.0)>0.0)
		{
			sec = sec -60.0;
		}
//		printf("[%02d:%5.2f]\n",minnut,sec);
		sprintf(play_time,"[%02d:%5.2f]\n",minnut,sec);
		s2 = 0.0;
	}
	return NULL;
}

void *get_message(void *arg)
{
	while(1)
	{
		if(my_lock == 1)
		{
			send_message("get_time_length\n");
			usleep(500000);
			send_message("get_time_pos\n");
			usleep(500000);
		}
	}
	return NULL;
}

void *send_songword(void *arg)
{
	FILE *fp;
	int ret,bit;
	char *p,*pt;
	char buf[100]={0};
	char s[50];
	char oldpath[50];
	while(1)
	{
		ret =strlen(pn->path);
		strcpy(oldpath,pn->path);
		strcpy(s,pn->path);
		s[ret - 3] = 't';
		s[ret - 2] = 'x';
		s[ret - 1] = 't';
		pt = s;
		fp = fopen(pt,"r");
		if(fp == NULL)
		{
			sleep(2);
//			perror("open file");
			continue;
//			return NULL;
		}
		while(!feof(fp))
		{
//			printf("%s\n",s);
			if((bit = strncmp(s,pn->path,ret - 4))!=0)
			{
				sleep(1);
				memset(s,0,sizeof(s));
				fclose(fp);
				break;
			}
			else
			{
				fgets(buf,100,fp);
				while(strncmp(play_time,buf,10)<0)
				{
					if((bit = strncmp(s,pn->path,ret - 4))!=0)
					{
						memset(buf,0,sizeof(buf));
						sleep(1);
						break;
					}
					sleep(0.5);
				}
				p = buf;
				p = p + 10;
				printf("%s\n",p);
				memset(buf,0,sizeof(buf));
			}
		}
		while(!strncmp(s,pn->path,ret - 4))
			sleep(1);
		fclose(fp);
	}
	return NULL;
}
/*void *send_songword(void *arg)
{
	while(1)
	{
	if(strncmp(pn->name,"ghostinmind",10)==0)
	{
		if(t>2.00 && t<3.00){
			printf("歌名：着魔\n");sleep(2);
		}else if(t>3.00 && t<=4.00){
			printf("演唱：张杰\n");sleep(2);
		}else if(t>5.00 && t<=6.00){
			printf("作词：跳舞，冀楚忱\n");sleep(2);
		}else if(t>7.00 && t<=8.00){
			printf("作曲：谭伊哲\n");sleep(2);
		}else if(t>=29.00 && t<30.00){
			printf("一瞬间 法则颠覆\n");sleep(2);
		}else if(t>=34.00 && t<35.00){
			printf("我是谁 是我心魔乱舞\n");sleep(2);
		}else if(t>=38.00 && t<39.00){
			printf("对与错 我能顿悟\n");sleep(2);
		}else if(t>=42.00 && t<43.00){
			printf("恶魔开始 让真理复苏\n");sleep(2);
		}else if((t>=46.00 && t<47.00)||(t>=125.00 && t<126.00)){
			printf("迷雾中 谁在低诉\n");sleep(2)
		}else if((t>=55.00 && t<56.00)||(t>=143.00 && t<144.00)){
			printf("用奋斗 去征服\n");sleep(2);
		}else if((t>=58.00 && t<59.00)||(t>=149.00 && t<150.00)){
			printf("踏平天地间的愤怒\n");sleep(2);
		}else if((t>=63.00 && t<64.00)||(t>=151.00 && t<152.00)){
			printf("云再黑 风再吼 不能让我停下征途风雨无阻\n");sleep(2);
		}else if((t>=71.00 && t<72.00)||(t>=159.00 && t<160.00)){
			printf("任脚下的 众神 为我 铺成一条英雄路\n");sleep(2);
		}else if((t>=81.00 && t<82.00)||(t>=166.00 && t<167.00)){
			printf("一滴泪 在 半路回头 我只有 战斗 战斗\n");sleep(2);
		}else if((t>=91.00 && t<92.00)||(t>=174.00 && t<175.00)){
			printf("满天星 在 坠落之后 我祈祷 别走 别走\n");sleep(2);
		}else if((t>=99.00 && t<100.00)||(t>=181.00 && t<182.00)){
			printf("那温度 已 无法保留 爱已经 冷透 冷透\n");sleep(2);
		}else if((t>=104.00 && t<05.00)||(t>=190.00 && t<191.00)){
			printf("我的心 愿 和你共有 一起到 尽头 尽头\n");sleep(2);
		}
	}
	}
	return NULL;
}*/

int song_play(Song *p)
{
	pipe(fd);
	if(access("song",F_OK) != 0 )
	{
		mkfifo("song",0666);
	}
	pid_t pid = vfork();
	if(pid < 0)
	{
		printf("fork error!\n");
		return 0;
	}
	else if (pid == 0)
	{
		close(fd[0]);
		dup2(fd[1],1);
		mplayer_pid = getpid();
		execl("/usr/bin/mplayer","mplayer","-slave","-quiet","-idle","-input","file=./song",p->path,NULL);
	}
	else
	{
		pthread_t r_tid;
		pthread_t g_tid;
		pthread_t s_tid;
		pthread_create(&r_tid,NULL,read_message,NULL);
		pthread_create(&g_tid,NULL,get_message,NULL);
		pthread_create(&s_tid,NULL,send_songword,NULL);
	}
	return 0;
}

void send_message(char *buf)
{
	int fd = open("song",O_WRONLY);
	write(fd,buf,strlen(buf));
	close(fd);
}

Song *play_song()
{
	int i,j=1;
	Song *pr;
	pr = read_song();
	printf("which song will you play:\n");
	scanf("%d",&i);
	getchar();
	my_lock = 1;
	while(pr != NULL)
	{
		if(i == j)
		{
			song_play(pr);
			pn = pr;
			return pr;
		}
		pr = pr->next;
		j++;
	}
	printf("NO have this song,you input wrong!\n");
	printf("enter any key to continue\n");
	getchar();
	return NULL;
}

/*Song *judge(char *p)
{
	Song *pr;
	while(pr->next !=NULL)
	{
		if(strcmp(p,pr->path)==0)
			break;
		pr = pr->next;
	}
	return pr;
}*/

Song *play_next(Song *p)
{
	char buf[60];
	Song *pr;
	pr = read_song();
	while(pr->next != NULL)
	{
		if(strcmp(p->path,pr->path)==0)
		{
			pr = pr->next;
			memset(buf,0,sizeof(buf));
			strcpy(buf,"loadfile ");
			strcat(buf,pr->path);
			strcat(buf,"\n");
			printf("%s\n",buf);
			getchar();
			send_message(buf);
			break;
		}
		else
		{
			pr = pr->next;
		}
	}
	pn = pr;
	return pr;
}

Song *play_last(Song *p)
{
	char buf[60];
	Song *pr;
	Song *pold;
	pr = read_song();
	pold = pr;
	while(pr!= NULL)
	{
		if(strcmp(p->path,pr->path)==0)
		{
			memset(buf,0,sizeof(buf));
			strcpy(buf,"loadfile ");
			strcat(buf,pold->path);
			strcat(buf,"\n");
			printf("%s\n",buf);
			getchar();
			send_message(buf);
			break;
		}
		else
		{
			pold = pr;
			pr = pr->next;
		}
	}
	pn = pold;
	return pold;
}
