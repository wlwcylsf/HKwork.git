#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "menu.h"
#include "songlist.h"
#include "songplay.h"

extern int mplayer_pid;

extern int my_lock;

int main()
{
	Song *pr;
	Song *p;
	Song *q;
	int i=0;
	int ch,loop = 1,a,aloop=1,b,bloop=1,c,cloop=1;
	char buf[40];
	char buff[10];
	while(loop)
	{
		ch = menu();
		switch(ch)
		{
			case 1:
			aloop=1;
			while(aloop)
			{
				a = listmenu();
				switch(a)
				{
					case 1:pr = read_song();
					add_song(pr);
					break;
					case 2:pr = read_song();
					add_songfile(pr);
					break;
					case 3:pr = read_song();
					remove_song(pr);
					break;
					case 4:pr = read_song();
					display_song(pr);
					break;
					case 0:aloop=0;break;
					default:aloop = 0;
				}
			}
			break;
			case 2:
			bloop=1;
			while(bloop)
			{
				b = playermenu();
				switch(b)
				{
					case 1:p = play_song();
					break;
					case 2:q = play_next(p);
					memset(p,0,sizeof(Song));
					p = q;
					break;
					case 3:q = play_last(p);
					memset(p,0,sizeof(Song));
					p = q;
					break;
					case 4:memset(buf,0,sizeof(buf));
					strcpy(buf,"seek 1\n");
					send_message(buf);
					break;
					case 5:memset(buf,0,sizeof(buf));
					strcpy(buf,"seek -1\n");
					send_message(buf);
					break;
					case 6:i++;
					if(i%2==1)
						my_lock = 0;
					else if(i%2==0)
						my_lock = 1;
					memset(buf,0,sizeof(buf));
					strcpy(buf,"pause\n");
					send_message(buf);
					break;
					case 7:my_lock = 0;
					printf("%d\n",mplayer_pid);
					memset(buf,0,sizeof(buf));
					memset(buff,0,sizeof(buff));
					strcpy(buf,"kill -9 ");
					printf("input(kill -9 pid)\n");
					gets(buff);
					strcat(buf,buff);
					system(buf);
					break;
					case 8:memset(buf,0,sizeof(buf));
					strcpy(buf,"get_time_pos\n");
					send_message(buf);
					break;
					case 0:bloop = 0;break;
					default:bloop=0;
					}
				}
			break;
			case 3:
			cloop=1;
			while(cloop)
			{
				c = soundmenu();
				switch(c)
				{
					case 1:printf("input the num of sound(0~100):\n");
					memset(buf,0,sizeof(buf));
					memset(buff,0,sizeof(buff));
					gets(buff);
					strcpy(buf,"volume ");
					strcat(buf,buff);
					strcat(buf," 1\n");
					send_message(buf);
					break;
					case 2:memset(buf,0,sizeof(buf));
					strcpy(buf,"mute 1\n");
					send_message(buf);
					break;
					case 3:memset(buf,0,sizeof(buf));
					strcpy(buf,"mute 0\n");
					send_message(buf);
					break;
					case 0:cloop=0;break;
					default:cloop=0;
				}
			}
			break;
			case 0:loop = 0;
			break;
			default:
			loop =0;
		}
	}
}