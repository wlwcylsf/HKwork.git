#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <sys/wait.h>

int main()
{
	pid_t pid0 = fork();
	if(pid0 < 0)
	{
		perror("fork error\n");
		return 0;
	}
	else if(pid0 == 0)
	{
		execl("./e4","./e4",NULL);
	}
	pid_t pid1 = fork();
	if(pid1 < 0)
	{
		perror("fork error\n");
		return 0;
	}
	else if(pid1 == 0)
	{
		execl("./e5","./e5",NULL);
	}
	while(waitpid(pid0,NULL,0)==pid0)
	{
		printf("%d\n",pid0);
		execl("./e4","./e4",NULL);
	}
	while(waitpid(pid1,NULL,0)==pid1)
	{
		printf("%d\n",pid1);
		execl("./e5","./e5",NULL);
	}

}