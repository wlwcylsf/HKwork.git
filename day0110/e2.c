#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <sys/wait.h>

int main()
{
	char *argv[]={"ls","-l",NULL};
//	scanf("%s %s",argv[0],argv[1]);
//	argv[2] = NULL;
	pid_t pid = vfork();
	if(pid < 0)
	{
		perror("fork error\n");
		return 0;
	}
	else if(pid == 0)
	{
		execv("/bin/ls",argv);
	}
	int status;
	pid_t pid1 = waitpid(pid,&status,1);
	if(WIFEXITED(status))
		printf("pid:%d,%d\n",pid1,WEXITSTATUS(status));
	else if(WTERMSIG(status))
		printf("pid:%d,%d\n",pid1,WIFSIGNALED(status));
	return 0;
}