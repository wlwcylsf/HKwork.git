#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

int main()
{
	signal(SIGCHLD,SIG_IGN);
	pid_t pid = fork();
	if(pid <0)
	{
		perror("fork error");
		return 0;
	}
	else if(pid == 0)
	{
		printf("hello world\n");
		exit(0);
	}
	for(int i=0;i<10;i++)
	{
		sleep(5);
	}
}