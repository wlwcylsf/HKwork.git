#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <time.h>
#include <sys/wait.h>

int main()
{
	for(int i = 0;i < 10;i++)
	{
		int j = rand()%10;
		srand(time(NULL));
		pid_t pid = vfork();
		if(pid ==0)
		{
			sleep(j);
			exit(1);
		}
		int status;
		pid_t pid1 = wait(&status);
		if(WIFEXITED(status))
			printf("myself:%4d father:%4d    child%d  sleep:%d\n",pid,getpid(),i,j);
		else if(WTERMSIG(status))
			printf("pid:%d,%d\n",pid1,WIFSIGNALED(status));
	}
	getchar();
}