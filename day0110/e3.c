#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <sys/wait.h>

int main()
{
	pid_t pid = fork();
	if(pid < 0)
	{
		perror("fork error\n");
		return 0;
	}
	else if(pid == 0)
	{
		for(int i=0;i<5;i++)
		{
			printf("hello world\n");
			sleep(1);
		}
		exit(2);
	}
	getchar();
	int status;
	pid_t pid1 = wait(&status);
	if(WIFEXITED(status))
		printf("pid:%d,%d\n",pid1,WEXITSTATUS(status));
	else if(WTERMSIG(status))
		printf("pid:%d,%d\n",pid1,WIFSIGNALED(status));
	return 0;
}