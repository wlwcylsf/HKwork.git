#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <sys/wait.h>

int main()
{

	for(int i = 0;i < 10;i++)
	{
		pid_t pid = fork();
		if(pid ==0)
		{
			printf("myself:%4d father:%4d    child%d\n",getpid(),getppid(),i);
			exit(1);
		}
	}
	printf("myself:%4d father:%4d\n",getpid(),getppid());
	getchar();
}