/*1000以内的整数，+100是完全平方数，再+168还是完全平方数*/

#include <stdio.h>
int main()
{
	int i,j,a,b;
	for(i=0;i<=1000;i++)
	{
		for(j=10;j<=40;j++)
		{
			a=j*j;
			if(a==i+100)
			{
				for(j=0;j<=40;j++)
				{
					b=j*j;
					if(b==i+268)
						printf("%d ",i);
				}
			}
		}
	}
	printf("\n");
	return 0;
}
