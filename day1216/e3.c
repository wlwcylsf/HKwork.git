/*输入两个正整数 m,n 求其最大公约数与最小公倍数*/
#include<stdio.h>

int main()
{
	int m,n,q,p,a,c;
	printf("input two num:");
	scanf("%d %d",&m,&n);
	c=m%n;
	a=m*n;
	while(c!=0)
	{
		m=n;
		n=c;
		c=m%n;
	}
	q=n;
	p=a/q;
	printf("max_divisor is %d,min_multiple is %d\n",q,p);
}
