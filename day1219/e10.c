#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void sort(float a[],int n)
{
	int i,j;
	float t;
	for(i=0;i<n-1;i++)
	{
		for(j=0;j<n-1-i;j++)
		{
			if(a[j]>a[j+1])
			{
				t=a[j];
				a[j]=a[j+1];
				a[j+1]=t;
			}
		}
	}
}


int main()
{
	float a[10];
	int i;
	srand(time(NULL));
	for(i=0;i<10;i++)
	{
		a[i]=rand()%100;
		printf("%.2f  ",a[i]);
	}
	printf("\n");
	sort(a,10);
	for(i=0;i<10;i++)
		printf("%3.2f  ",a[i]);
	printf("\n");
}
			
