#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int sum_row(int a[][6],int row,int col,int sumrow[],int g)
{
	int i,j;
	for(i=0;i<row;i++)
	{
		sumrow[i]=0;
		for(j=0;j<col;j++)
			sumrow[i]+=a[i][j];
	}
	return (sumrow[5]);
}

int main()
{
	int i,j;
	int a[5][6],sumrow[5];
	srand(time(NULL));
	for(i=0;i<5;i++)
	{
		for(j=0;j<6;j++)
		{
			a[i][j]=rand()%100;
			printf("%4d",a[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	sum_row(a,5,6,sumrow,5);
	for(i=0;i<5;i++)
		printf("%d\n",sumrow[i]);
}
