#include <stdio.h>
#include <math.h>

int isprime(int x)
{
	int i;
	for(i=2;i<=sqrt(x);i++)
		if(x%i==0)
			return 0;
	return 1;
}


int main()
{
	int i,j=0;
	for(i=3;i<=1100;i++)
	{
		if(isprime(i))
		{
			j++;
			printf("%5d",i);
			if(j%6==0)
				printf("\n");
		}
	}
	printf("\n");
}
