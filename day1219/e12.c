#include <stdio.h>

int fun1(int m,int n)
{
	if(n==1)
		return m;
	else 
		return m*fun1(m,n-1);
}

int main()
{
	int m,n;
	printf("input m,n:");
	scanf("%d%d",&m,&n);
	printf("%d^%d=%d\n",m,n,fun1(m,n));
}
