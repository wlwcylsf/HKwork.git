#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void change(int a[][3],int row,int col)
{
	int i,j,temp;
	for(i=0;i<row;i++)
		for(j=0;j<col;j++)
			if(i<j)
			{
				temp=a[i][j];
				a[i][j]=a[j][i];
				a[j][i]=temp;
			}
}

int main()
{
	int i,j;
	int a[3][3];
	srand(time(NULL));
	for(i=0;i<3;i++)
	{
		for(j=0;j<3;j++)
		{
			a[i][j]=rand()%100;
			printf("%3d",a[i][j]);
		}
		printf("\n");
	}
	printf("*******************");
	printf("\n");
	change(a,3,3);	
	for(i=0;i<3;i++)
	{
		for(j=0;j<3;j++)
		{
			printf("%3d",a[i][j]);
		}
		printf("\n");
	}
}
