#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

void sort(int a[],int n)
{
	int i,j;
	int t;
	for(i=0;i<n-1;i++)
		for(j=0;j<n-1-i;j++)
			if(a[j]>a[j+1])
			{
				t=a[j];
				a[j]=a[j+1];
				a[j+1]=t;
			}
}

int isprime(int x)
{
	int i;
	for(i=2;i<=sqrt(x);i++)
		if(x%i==0)
			return 0;
	return 1;
}


int main()
{
	int a[20],b[20];
	int i,j;
	srand(time(NULL));
	for(i=0;i<20;i++)
	{
		a[i]=rand()%100;
		printf("%4d",a[i]);
	}
	printf("\n");
	
	for(i=0,j=0;i<20;i++)
	{
		if(isprime(a[i]))
		{
			b[j]=a[i];
			printf("%4d",a[i]);
			j++;
		}
	}
	printf("\n");
	sort(b,j);
	for(i=0;i<j;i++)
		printf("%4d",b[i]);
	printf("\n");
}
