#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>

void mycat(char *name)
{
	char a[1000];
	DIR *dir;
	struct dirent *ptr;
	struct stat buf;
	dir = opendir(".");
	while((ptr = readdir(dir)))
	{
		if(strcmp(name,ptr->d_name)==0)
		{
			stat(ptr->d_name,&buf);
			int fp = open(ptr->d_name,O_RDONLY);
			int ret = read(fp,a,buf.st_size);
			a[buf.st_size] = '\0';
			printf("%d %s\n",ret,a);
		}
	}
}

int main()
{
	mycat("123.txt");
}