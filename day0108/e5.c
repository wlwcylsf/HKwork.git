#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main()
{
	int fp = open("1.txt",O_RDWR);
	int fd = open("2.txt",O_RDRW);
	struct flock lock;
	lock.l_type = F_WRLCK;
	lock.l_start = 0;
	lock.l_whence = SEEK_SET;
	lock.l_len = 0;

	printf("start writelock to 1.txt\n");
	fcntl(fp,F_SETLKW,&lock);
	printf("1.txt writelock success\n");
	getchar();

	printf("start writelock to 1.txt\n");
	fcntl(fd,F_SETLKW,&lock);
	printf("1.txt writelock success\n");
	getchar();

	lock.l_type = F_UNLCK;
	fcntl(fp,F_GETLK,&lock);
	printf("no writelock\n");

	close(fp);
	close(fd);
}
