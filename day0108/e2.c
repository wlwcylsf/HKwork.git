#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <time.h>

void my_rmdir(char *path)
{
	char apath[1000];
	DIR *dir;
	struct dirent *ptr;
	struct stat buf;
	dir = opendir(path);
	while((ptr = readdir(dir)))
	{
		stat(ptr->d_name,&buf);
		if(S_ISDIR(buf.st_mode))
		{
			strcpy(apath,path);
			strcat(apath,"/");
			strcat(apath,ptr->d_name);
			my_rmdir(apath);
			rmdir(ptr->d_name);
		}
		else if(S_ISREG(buf.st_mode))
			remove(ptr->d_name);
		else
			rmdir(ptr->d_name);
	}
}
int main()
{
	my_rmdir("./11");
	return 0;
}