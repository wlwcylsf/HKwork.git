#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <time.h>


void readfile(char *path)
{
	char apath[1000];
	DIR *dir;
	struct dirent *ptr;
	struct stat buf;
	dir = opendir(path);
	printf("%s\n",path);
	while((ptr = readdir(dir)))
	{
		stat(ptr->d_name,&buf);
		if(ptr->d_type == 4 && strncmp(ptr->d_name,"..",2) !=0 && strncmp(ptr->d_name,".",1) !=0)
		{
			memset(apath,0,sizeof(apath));
			strcpy(apath,path);
			strcat(apath,"/");
			strcat(apath,ptr->d_name);
			readfile(apath);
			printf("%10s%10lu  %s\n",ptr->d_name,buf.st_size,ctime(&buf.st_mtime));
		}
		else if(strncmp(ptr->d_name,"..",2) !=0 && strncmp(ptr->d_name,".",1) !=0)
			printf("%10s%10lu  %s\n",ptr->d_name,buf.st_size,ctime(&buf.st_mtime));
	}
	closedir(dir);
}
int main()
{
	readfile(".");
	return 0;
}
