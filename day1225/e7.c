#include <stdio.h>
int *findmax(int *s,int t,int *k)
{
	int i;
	for(i=0;i<t;i++)
		if(*(s+i)>*s)
		{
			*s = *(s+i);
			*k = i;
		}
	return s;
}

void main()
{
	int a[10]={12,23,34,45,56,67,78,89,11,22},k,*add;
	add=findmax(a,10,&k);
	printf("%d,%d,%p\n",a[k],k,add);
}
