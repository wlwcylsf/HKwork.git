#include <stdio.h>

int main()
{
	int a[3][2]={2,4,6,8,10,12},(*p)[2],i,j;
	p = a;
	for(i=0;i<3;i++)
		for(j=0;j<2;j++)
			printf("%d ",*(*(p+i)+j));
	printf("\n*************\n");
	for(i=0;i<2;i++)
		for(j=0;j<3;j++)
			printf("%d ",*(*(p+j)+i));
	printf("\n");
}
