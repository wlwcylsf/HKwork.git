#ifndef _SONGLIST_H_
#define _SONGLIST_H_

struct songlistnode
{
	int num;
	char name[20];
	char path[30];
	struct songlistnode *next;
};

typedef struct songlistnode Song;

Song *read_song();

int display_song(Song *head);

int write_song(Song *r);

int rewrite_song(Song *head);

int add_song(Song *head);

int add_songfile(Song *head);

int remove_song(Song *head);

#endif