#include <stdio.h>
#include <stdlib.h>

int menu()
{
	int ch;
	system("clear");
	printf("\t\t\t==========MUsic Mplayer=========\n");
	printf("\t\t\t================================\n");
	printf("\t\t\t          1. music list\n");
	printf("\t\t\t          2. music player\n");
	printf("\t\t\t          3. music sound\n");
	printf("\t\t\t          0. quit\n");
	printf("\t\t\t================================\n");
	printf("select:");
	scanf("%d",&ch);
	getchar();
	while(ch < 0||ch > 3)
	{
		printf("you inout wrong,please input again\n");
		scanf("%d",&ch);
		getchar();
	}
	return ch;
}

int listmenu()
{
	int ch;
	system("clear");
	printf("\t\t\t==========   listmenu  =========\n");
	printf("\t\t\t================================\n");
	printf("\t\t\t          1. add song\n");
	printf("\t\t\t          2. add song file\n");
	printf("\t\t\t          3. remove song\n");
	printf("\t\t\t          4. show all songs\n");
	printf("\t\t\t          0. quit\n");
	printf("\t\t\t================================\n");
	printf("select:");
	scanf("%d",&ch);
	getchar();
	while(ch < 0||ch > 4)
	{
		printf("you inout wrong,please input again\n");
		scanf("%d",&ch);
		getchar();
	}
	return ch;
}

int soundmenu()
{
	int ch;
	system("clear");
	printf("\t\t\t==========   soundmenu  =========\n");
	printf("\t\t\t================================\n");
	printf("\t\t\t          1. control sound\n");
	printf("\t\t\t          2. no sound\n");
	printf("\t\t\t          3. non sound\n");
	printf("\t\t\t          0. quit\n");
	printf("\t\t\t================================\n");
	printf("select:");
	scanf("%d",&ch);
	getchar();
	while(ch < 0||ch > 3)
	{
		printf("you inout wrong,please input again\n");
		scanf("%d",&ch);
		getchar();
	}
	return ch;
}

int playermenu()
{
	int ch;
	system("clear");
	printf("\t\t\t==========   playermenu  =========\n");
	printf("\t\t\t================================\n");
	printf("\t\t\t          1. play song\n");
	printf("\t\t\t          2. next song\n");
	printf("\t\t\t          3. last song\n");
	printf("\t\t\t          4. quick song\n");
	printf("\t\t\t          5. requick song\n");
	printf("\t\t\t          6. stop/again song\n");
	printf("\t\t\t          7. stop mplayer\n")
	printf("\t\t\t          0. quit\n");
	printf("\t\t\t================================\n");
	printf("select:");
	scanf("%d",&ch);
	getchar();
	while(ch < 0||ch > 7)
	{
		printf("you inout wrong,please input again\n");
		scanf("%d",&ch);
		getchar();
	}
	return ch;
}